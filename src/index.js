import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// метод render позволяет указать в какой элемент страницы развернуть 
// он говорит - вызови <App />,
ReactDOM.render(<App />, document.getElementById('root'));
